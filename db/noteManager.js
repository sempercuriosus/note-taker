// #region NOTE
//

/*
    * Takes in the note data
    *   reads
    *   writes
    *   deletes
    * based on the api http methods used
    * creates a new id for the note.
    *   probably a guid (UUID) generator
*/

//
// #endregion NOTE


// #region imports
//

const fs = require("fs");
const util = require("util");

// adding this package to avoid having to write promises
// read and write, default are async
const fileReader = util.promisify(fs.readFile);
const fileWriter = util.promisify(fs.writeFile);

//
// #endregion imports

/**
 * @name Note   
 * @classdesc represents what the notes actions that can be taken upon the notes
 * 
 * Read, 
 * Write, or
 * Delete
 * 
 * This is NOT a constructor function, so an import is needed to access the functions
 * 
 */
class Note {
    // Function List in Note Class
    //

    /**
    * @name read
    * @param 
    * @returns the db file as a string
    */
    read () {
        // i tried making this work within a TRY CATCH block, but that was not working and I do not know why yet...
        // ANSWER: it was not the TRY CATCH block. I was assigning this to a const, then returning. 
        try {
            return fileReader("db/db.json") || "";
        }
        catch {
            console.error("could not read from db file...");
        }
    };


    /**
    * @name write
    * @classdesc writes the db file to the disk
    * @param textToWrite
    */
    write (textToWrite) {
        /* Desc
            * writes the db file to the disk
            * 
            * needs to be the same type of operation as read()
        */

        try {
            const stringifiedNote = this.stringifyText(textToWrite);
            return fileWriter("db/db.json", stringifiedNote); // not adding a default here. I would not want to overwrite the existing file
        } catch {
            console.error("could not write to db file...");
        }
    };


    /**
    * @name getNoteDBFile
    * @classdesc this accesses the other functions needed to get, parse, and return the db file 
    * @returns a string of the JSON file
    */
    getNoteDBFile () {
        /* Desc
            * gets the db file 
            *
        */

        return this.read()
            .then((db_file) => {
                let noteString = "";
                try {
                    noteString = this.parseJSON(db_file);
                }
                catch {
                    noteString = [];
                }
                return noteString;
            });
    };



    /**
    * @name deleteNote
    * @classdesc accepts a note id and deletes the note
    * @param noteId
    * @returns 
    */
    deleteNote (noteId) {
        /* Desc
            * accepts a note id 
            * loads the existing notes
            * deletes the note from the array by a filter
            * 
        */
        return this.getNoteDBFile()
            // used this from a previous example from the project 1 of something I wrote. 
            .then((notes) => notes.filter(note => note.id !== noteId))
            .then((updatedNotes) => this.write(updatedNotes))
            .then(() => console.info("deleted note", noteId));
    };


    /**
    * @name addNoteDBFile
    * @classdesc updates the note db file with the most recent note addition
    * @param newNote
    * @returns 
    */
    addNoteDBFile (noteToAdd) {
        if (noteToAdd) {
            // descructure the note object
            // title and note are key names in the defined object
            let { title, text } = noteToAdd;

            // must have a title and text to add the note. if not then nothing is added. 
            // inform the user there was nothing added
            if (!title && !text) {

                let errorText = "";
                if (!title) {
                    errorText = "You must have a title for your note to add it.";
                }
                else if (!text) {
                    errorText = "You must have text in your note to add it.";
                }
                else {
                    errorText = "You must have a title and text to add a note.";
                }
                throw new Error(errorText);
            }

            // create a new GUID (UUID) to use as the ID for a note
            const id = this.makeGUID();

            // make a new note
            const newNote = { title, text, id };

            // where the magic happens
            return this.getNoteDBFile()
                // get the existing notes from the promise with getNoteDBFile
                // returning the notes file as a promise 
                // taking the existing notes, using Spread (remember why this is Spread -- because it is not in a function delcaration), and merging the new note into the existingNotes
                .then((existingNotes) => {
                    // this is interesting. without the addition of the info to the console, the app does not return my finished note.
                    return [ ...existingNotes, newNote ];
                })
                // passing the updatedNotes into the write, updating the db file with the new addition
                .then((updatedNotes) => {
                    this.write(updatedNotes);
                })
                // return the new note up the promise chain
                .then(() => newNote);

        }
    };

    /**
    * @name parseJSON
    * @classdesc parse JSON
    * @param {Object} objectToParse
    * @returns 
    */
    parseJSON (objectToParse) {
        /* Desc
            * parse JSON
            *
        */

        let parsedNotes;

        try {
            parsedNotes = JSON.parse(objectToParse);
        }
        catch {
            parsedNotes = [];
        }

        return parsedNotes;

    };


    /**
    * @name makeJSONString
    * @param {JSON} jsonToStringify takes in a JSON object and 
    * @returns string of the passed in data
    */
    stringifyText = (jsonToStringify) => {
        if (jsonToStringify) {
            const jsonAsString = JSON.stringify(jsonToStringify) || "";

            return jsonAsString;
        }
    }; //  [ end : makeJSONString ]




    /**
    * @name makeGUID
    * @returns a new GUID (UUID) version 4. 
    */
    makeGUID = () => {
        return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (currentChar) {
            // convert to hex number, numerical base 16, 4 bits
            const convertTo = 16;
            // get a random digit from 0 to 15 
            // 16 total options to correspond to the hex number base 
            const randomNumber = Math.random() * 16 | 0;
            /*
                * creates a random hex digit to substitute into the return string above
                * this subsitutes x and y with the hex char created
                *   0x3 0011 isolates the last two bits of the randomNumber when ANDed
                *   0x8 1000 sets the first bit to 1
                * expaning the range effectively from (two bits) 3, in base 10, up to (4 bits) 11, in base 10, to cover the entire range of possible hex characters, which is also represented by 4 bits.
            */
            const returnValue = (currentChar === 'x') ? randomNumber : (randomNumber & 0x3 | 0x8);

            // return the string as a hex string, base 16
            return returnValue.toString(convertTo);
        });
    }; //  [ end : makeGUID ]
};


/*
  * Export Module
*/

module.exports = new Note();