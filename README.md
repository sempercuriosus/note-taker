# Note Taker App

The purpose of this application is to take notes and save them to a notes file on the computer in a json file.

## Table of Contents

- [About The Project and Features](#about-project)
- [Built With](#built-with)
- [Getting Started](#getting-started)
  - [Prerequisites And Dependencies](#prerequisites-and-dependencies)
  - [Installation](#installation)
- [Deployment Location](#deployment-location)
- [Challenges](#challenges)
- [Acknowledgments](#acknowledgments)
- [Author Credit](#author-credit)
- [Final Note](#final-note)

===

## About The Project and Features<a id="about-project"></a>

- The application allows you to add a title, to describe your note
- Add note text containing whatever you want to note
- Save to a `JSON` file for persistent storage
- The saved notes are listed from, oldest to newest, in a recallable list to view the previous notes. They are not editable.

### Built With<a id="#built-with"></a>

- Javascript
  - Node.js
  - Express.js

**Starter Code (Code Provided for the project) has**

- HTML
- CSS
- Javascript

---

## Getting Started<a id="getting-started"></a>

- Most of the js code files contain a #region section called `NOTE` these describe my thought as I was trying to develop the application
- I left a lot of comments in my code for the same reason -- to inform those reading this later on.

### Prerequisites And Dependencies<a id="prerequisites-and-dependencies"></a>

- **dev**
  - `nodemon` version `^3.0.1`
  - `jest` version `^29.7.0`
- **required**
  - `express` version `^4.18.2`

### Installation<a id="#installation"></a>

- `npm i` or `npm install` can be used to get the dependencies

### Testing

- I did include only one test, with `Jest`, to check that the guid is in guid form factor
- That can be run with `npx jest`

### Running The App

- This is a fully functional web app, deployed at the location below, so running the app is done by accessing that location.
  - This app is deployed with Heroku

---

## Deployment Location<a id="deployment-location"></a>

[Live Link]()

At the time of making this app the link may not be included or functional. I have been having a lot of support related issues with the cloud service used. These, unfortunately, are not in my control, but I am actively working on them yet.

- Date updated: **10 OCT 2023**

### Examples

#### Landing Page View

- This is the screen you will see when navigating to the application.
  ![Landing Page](readme-screenshots/2023-10-oct-landingPage.png)

#### Note View Without Notes

- A blank note page is what you will see the first time you go to add a note, or if you have cleared out all of your previous notes.
  ![View with No Notes](readme-screenshots/2023-10-oct-noNotes.png)

#### Example Note Creation Preview

- Here is what adding a note may look like. I put a lot of text here to show that there is a large amount of text that one can add.
  ![Adding a Note](readme-screenshots/2023-10-oct-addingANote.png)

#### Note List View

- This is what a having one note looks like in your list, and as noted, the notes are ordered by their creation, oldest to newest on bottom.
  ![Note List with Notes](readme-screenshots/2023-10-oct-noteList.png)

#### Note Recall View

- If you recall a note, you can see the information presented in the note pane, and note that the text is slightly grayed out. This indicates that the notes are not available for edit as of this version.
  ![Note Recall View](readme-screenshots/2023-10-oct-noteRecall.png)

---

## Challenges<a id="challenges"></a>

- Getting the Router() routes up for the first time on my own.
  - This is a pretty cool concept to keep concerns separate
- Understanding that no matter how excited you may be about your code working as you imagined it to the terminal (MacOS), even in a commit, will print the last command from all even numbers of `!!` and that `!"` escapes the double quote for some reason
- promise chains and returning data up the chain... this wow. I kept trying to assign things like fileReader("db/db.json") to a variable and then return them, but that will not work in an async nature.
- The returns, I think, were going to the function caller, and not up the promise chain itself.
  - Mostly, I think this is from having only every written synchronous code.
  - I kept getting undefined reference errors or type errors when trying to work with what ended up being a promise object.
- Not renaming anything from supplied code
  - This is good practice, but what I really started to understand this time around was reviewing the code you may need in more detail.
  - What I did was not see that there were reference to the title, text, and Id on the front end -- I named these things noteTitle, noteText, and noteId.
  - As one can imagine, this caused a lot of confusion as to why the app was not displaying or working with notes. I was able to conquer this by the devs good 'ol friend `console.log()` after tracking the issue, to the appropriate function, in the error stack.

---

## Acknowledgments<a id="acknowledgments"></a>

- I have used the `solved` code as a guide when stuck, and I did not outright copy any code in `/solved`.
- I think my `deleteNote` is similar enough to the function in the starter code to note this specifically, but I think this is the most efficient way to handle that action. I have used that before in the [Project 1](https://github.com/VonjareeW/FitnessTracker/blob/194ada059bab42d1944e1f05a9d6fac7a87750e1/assets/script.js#L336) code as well.
  - Here is the snippet from that
    - `workouts = workouts.filter(workout => workout.name + " " + workout.date != selectedWorkout);`
- The starter code -- this was key for me this time. Having this as a guide for when I got stuck really helped show me what was needed.
- Chat GPT helped me debug
- Chat GPT gave me the base idea for the function that makes the guid `makeGUID()`. The notes, however, are mine.

---

## Author Credit<a id="author-credit"></a>

- Eric Hulse [semper curiosus](https://github.com/sempercuriosus)

---

===

## Final Note<a id="final-note"></a>

- This was a a lot more difficult for me as this is the first time I really have sat down to write this style of code. With that in mind, I am proud of the job that I did here, and I am really excited to continue learning these concepts!
- I am starting to understand APIs and Routes, I think that I am really understand Javascript now too!
- Async communication is still not clicking for me, meaning I have not had the right breakdown of it yet.
- The Promise Chains at first really threw me for a loop.

---

Here is my own definition of **knowledge**.

`Knowledge is not the same as understanding -- knowledge with action that is understanding. It is not enough to just know -- seek to understand.`

I have no doubts I will understand the challenges I have had. I need to act on the knowledge a little more.

Take Care,

EH October 7, 2023

---

===

