// #region NOTE
//
/*
    * This is for routes and actions related to the notes themselves
*/

//
// #endregion NOTE

// #region imports
//

const express = require("express");
const notes = express.Router();
const NoteManager = require("../db/noteManager");

//
// #endregion imports

// #region routes
// needs to have a get, post and optional delete method
/*
 * These are the routes for the notes api
 * 
 * note, none of the paths have `/api` in the front of them. as that is defined in the app instance in server.js
*/

// GET 
notes.get("/notes", (req, res) => {
    // #region get description

    /*
    * this needs to return the notes in the db file 
    * 
    * create an instance of the noteManager
    * that is responsible for 
    *   getting the db file
    *   responding with the db file
    * 
    * 
    * all these route's actions will have to be async now. 
    *   this is not a linear operation anymore 
    *   sync actions would not work.
    * 
    * generally, I can take in the response, and return an error if needed
    * 
    */

    // #endregion get description

    NoteManager
        .getNoteDBFile()
        // return the notes found as json
        .then((notes) => res.json(notes)) // had logs here, but this syntax is too clean not to use
        .catch((getError) => res.status(500).json(getError))
        ;
});


// POST
notes.post("/notes", (req, res) => {
    // #region post description

    /*
        * this needs to take in the new note (singular new note), update the file and reload the notes
        * 
        * 
        * create an instance of the noteManager
        * that is responsible for 
        *   taking in the request (req) body as JSON
        *   opening the db file
        *   appends the new note to the db file
        * return ALL notes to the client
        * 
        * client will then update
        * 
    */

    // #endregion post description
    const newNote = req.body;

    NoteManager
        .addNoteDBFile(newNote)
        .then((note) => {
            return res.json(note);
        })
        .catch((postError) => res.status(500).json(postError));
    ;

    // res.send("received Notes POST request");
});


// DELETE
notes.delete("/notes/:id", (req, res) => {
    // #region delete description

    /*
        * (This is optional)
        * takes in a query param :id
        * deletes the note with the corresponding id
        * reads the db file, finds the note, deletes it, saves the db file again.
        * 
        * found this awesome source https://youtu.be/SccSCuHhOw0?si=IaQ33TutekClpm7U&t=961
        * 
    */

    // #endregion delete description

    // this was erroring because I was treating res.send() as if it were a clog.
    // res.send("notes deleted " + req.params.id);
    NoteManager
        .deleteNote(req.params.id)
        .then(() => res.json({ "return": "deleted" }))
        .catch((deleteError) => res.status(500).json(deleteError));

});

//
// #endregion routes



// Export module
module.exports = notes;