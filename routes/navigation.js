// #region NOTE
//
/*
    * This is for navigation to and from the web pages
    *
    *
*/

//
// #endregion NOTE

// #region imports
//
const path = require("path");
const express = require("express");
const nav = express.Router();
//
// #endregion imports

// routes

// starting with the most specific, such that, the wildcard will not pick up the traffic (for cardinality)
nav.get("/notes", (req, res) => {
    res.sendFile(path.join(__dirname, "../public/notes.html"));
});

// everything else goes to index
nav.get("*", (req, res) => {
    res.sendFile(path.join(__dirname, "../public/index.html"));
});

// export the module
module.exports = nav;