// What do I need?
/*
    * express for sure
    * need routes to get around the website
    * 
*/

const express = require("express");
const PORT = process.env.PORT || 3001;

// Initialize app
const app = express();

// Initialize navigation
const navigation = require("./routes/navigation");

// Initialize notes
const notes = require("./routes/notes");

// do i need middleware for the app/processing? 
// thinking that i will since are working with json.
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(express.static('public'));

// routes

// nav (up higher for cardinality)
// notes
app.use("/api", notes);

// use, in this context, is going to use the app i defined
app.use("/", navigation);

// need to listen on PORT
// why is PORT in caps lock?

app.listen(PORT, () => console.log("The Notes app is listening on", PORT, "http://localhost:" + PORT));