const Note = require("../db/noteManager");


/** Will see if the newly made guid is of an okay format  */
describe("Note GUID", () => {
    // TEST
    test("Tests if the GUID generated is in the expected form", () => {
        let errFlag = false;
        const guidTemplate = "xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx";
        const guidTemplateParts = guidTemplate.split("-");
        const guid = Note.makeGUID();
        const guidParts = guid.split("-");

        console.info(guidTemplateParts, guidParts);
        console.info(guid);

        for (const i of guidTemplateParts) {
            /*
              * if the errFlag is true this will fail
                * checking that the length of the parts is the same, not null, empty, or undefined
            */
            errFlag = ((guidTemplateParts.length === guidParts.length) && (guidParts[ i ] !== "" || guidParts[ i ] !== undefined || guidParts[ i ] !== null)) ? false : true;
        }
        expect(errFlag).toEqual(false);
    });
});